<?php 

//these lines are telling php to report all errors if there are
error_reporting(E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

//variables with the directory names and the desired file name
$tmp_dir = "uploads/tmp/";
$target_file = "archive.zip";
$target_dir = "uploads/";

$return = 0;

//constants
define("FNAME", "firm"); //value of the name property of the input tag of the html script
define("EXTENSION", "zip");//extension expected for the archive

define("FIRMWARE", "firmware.bin");//expected filename for the firmware
define("PARTITION", "partitions.bin");//expected filename for the partitions

//here, we check if the temporary directory exists and if not, we create it.
//the return value of the command is stored in $return and can be checked to see if the command failed
if (!file_exists($tmp_dir)) {
	echo "Target directory not detected, creating ...";
	exec("mkdir -p ".$tmp_dir, $output, $return);
}

//if the craetion fails (the directory doesnt exists and can't be created)
//abort the script and display error
if ($return !== 0) {
	die("Couldn't create target directory, aborting\n");
}
//if no file has been submited,      
//abort the script and display error
if(! isset($_POST["submit"])) {
	die("No file upload detected<br>");
}

//if no file with the correct name property has been sent,
//abort the script and display error
if (! $_FILES[FNAME]) {
	die("Wrong field error, did you use a bad script ?<br>");
}

//retrieve the filename and extension for some tests
$filename = $_FILES[FNAME]["name"];
$file_type = strtolower(pathinfo($filename,PATHINFO_EXTENSION));

echo "Target file : ".$target_file."<br>";

echo "File upload detected : ".$filename." ...<br>";

//if file is not the correct extension,
//abort the script and display error
if ($file_type !==  EXTENSION ) {
	die("Not a valid ".EXTENSION." file : ".$filename."<br>");
}

echo "Valid ".EXTENSION." file ...<br>";

//if moving the file doesn't work, 
//abort the script and display error
if (! move_uploaded_file($_FILES[FNAME]["tmp_name"], $tmp_dir.$target_file)) {
	die("Couldn't save the file<br>");
}

echo "The file has successfully been saved ... <br>";

//unzips the archive in the temporary directory
exec("unzip -d ".$tmp_dir." ".$tmp_dir.$target_file, $output, $return);

if ($return !== 0) {
	die("Unzipping archive failed ...<br>");
}

echo "Unzipping successfull<br>";

//Here, we check if the firmware and partitions are here and if they are not,
//we delete everything in the temporary folder,
//abort the script and display error
if ((!file_exists($tmp_dir.FIRMWARE)) || (!file_exists($tmp_dir.PARTITION))) {
	if (!file_exists($tmp_dir.FIRMWARE)) {
		echo "missing firmware :".FIRMWARE."<br>";
	}

	if (!file_exists($tmp_dir.PARTITION)) {
		echo "missing partitions :".PARTITION."<br>";
	}

	exec("rm -f ".$tmp_dir."*", $output, $return);
	if ($return !== 0) {
		echo "Couldn't clear tmp directory<br>";
	}
	die();
}

//if the files have the correct filename, we can move them to the final folder
exec("mv ".$tmp_dir.FIRMWARE." ".$tmp_dir.PARTITION." ".$target_dir, $output, $return);

//we still erase everything from the temporary folder to prevent attacks.
//
//Otherwise, a malicious user could upload some php files with the binaries and try to reach them 
//And that could lead to code injections
if ($return !== 0) {
	echo "An error occured while moving the files<br>";

	exec("rm -f ".$tmp_dir."*", $output, $return);
	if ($return !== 0) {
		echo "Couldn't clear tmp directory<br>";
	}
	die();
}

//and finally, we can upload the binaries to the esp32
exec("uploads/script.sh", $output, $return);

if  ($return === 0) {
	die("Upload successfull<br>");
}
die("Upload failed<br>");

?>
